// Document ready
$(function(){
    $(".main_menu a").hover(function(){
        // Set the title to nothing so we don't see the tooltips
        $(this).attr("title","");
    });
    $('#search').prop('placeholder', 'Search something...');
    //
    $('.search-block').on('click','.search-button', function(){
        $('.desktop #main-menu').toggle();
        $('.tablet .logo').toggle();
        $('.search-block').toggleClass('flex_1 flex_8 t-flex_1 t-flex_6');
        $(this).toggleClass('flex_1 t-flex_1 icon-search icon-close');
        $('#search-form').toggleClass('flex_11 flex t-flex_5');
    });
});